# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2023-11-05 12:39+0000\n"
"PO-Revision-Date: 2017-01-20 18:22+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#. type: Content of: <div><h2>
#: ubiquity-slideshow/slides/kde.html:3
msgid "One Blue Marble - a community of creators around the globe"
msgstr "En blå pärla, en gemenskap av kreatörer runt hela jordklotet"

#. type: Content of: <div><h2>
#: ubiquity-slideshow/slides/neon.html:3
msgid "KDE neon - the latest and greatest from the KDE community"
msgstr "KDE-neon, det senaste och bästa från KDE: s gemenskap"

#. type: Content of: <div><h2>
#: ubiquity-slideshow/slides/plasma.html:3
msgid "Plasma Desktop, simple by default and powerful when needed"
msgstr "Plasma skrivbord, normalt enkelt och kraftfullt vid behov"

#. type: Content of: <div><h2>
#: ubiquity-slideshow/slides/secure.html:3
msgid "Security, privacy and autonomy with KDE, Plasma and Neon"
msgstr "Säkerhet, integritet och autonomi med KDE, Plasma och Neon"
