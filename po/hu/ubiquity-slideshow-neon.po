# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@fsf.hu>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2023-11-05 12:39+0000\n"
"PO-Revision-Date: 2022-01-14 11:59+0100\n"
"Last-Translator: Kristóf Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.07.70\n"

#. type: Content of: <div><h2>
#: ubiquity-slideshow/slides/kde.html:3
msgid "One Blue Marble - a community of creators around the globe"
msgstr "Egy kék golyó - alkotók közössége a világ minden tájáról"

#. type: Content of: <div><h2>
#: ubiquity-slideshow/slides/neon.html:3
msgid "KDE neon - the latest and greatest from the KDE community"
msgstr "KDE Neon - a legfrissebb és legnagyszerűbb a KDE közösségtől"

#. type: Content of: <div><h2>
#: ubiquity-slideshow/slides/plasma.html:3
msgid "Plasma Desktop, simple by default and powerful when needed"
msgstr "Plasma asztal, többnyire egyszerű, de hatékony, ha szükséges"

#. type: Content of: <div><h2>
#: ubiquity-slideshow/slides/secure.html:3
msgid "Security, privacy and autonomy with KDE, Plasma and Neon"
msgstr "Biztonság, adatvédelem és autonómia a KDE-vel, Plasmával és Neonnal"
